{% extends "base" %}

{% block head %}
{{ super() }}
<script
  defer
  src="/static/js/ace-min-noconflict/ace.js?v={{ resources_version }}"
  integrity="sha384-pHolRD674CW03iJeuvwFcYxjrYHl7uBsv43EFlDLWhiCCLmhsGL/jveQRniHL+af"></script>
<script
  defer
  src="/static/js/ace-min-noconflict/ext-modelist.js?v={{ resources_version }}"
  integrity="sha384-/whn+QM+slQwsIU53M8dabpXei0lk1uWLg1Wvit+S/i4zAnviTAG5ldiTy3stmNC"></script>
<script
  defer
  src="/static/js/editor.js?v={{ resources_version }}"
  integrity="sha384-P5nWRBFaIkYyI4jajiya/75d5Ajad+eIktSJKJcKNuupqyRDX3m2XxPjlnynZFhW"></script>
{% endblock head %}

{% block title %}
New
{% endblock title %}

{% block header_title %}
New paste
{% endblock header_title %}

{% block header_subtitle %}
Create a new paste.
{% endblock header_subtitle %}

{% block main %}
<form id="paste_upload" action="/pastes" method="post">
  <input type="hidden" name="anti_csrf_token" value="{{ session.data.anti_csrf_token }}"/>
  <div class="field is-grouped">
    <div class="control is-expanded">
      <label class="label">Paste name</label>
      <input
        name="name"
        class="input"
        type="text"
        placeholder="Paste name"/>
    </div>
    <div class="control has-icons-left">
      <label class="label">Visibility</label>
      <div class="select">
        <select name="visibility">
          <option>public</option>
          <option selected>unlisted</option>
        {% if user %}
          <option>private</option>
        {% endif %}
        </select>
        <span class="icon is-small is-left">
          <i class="fas fa-globe"></i>
        </span>
      </div>
    </div>
  </div>

  <div class="field">
    <label class="label">Description</label>
    <div class="control">
      <input
        name="description"
        class="input"
        type="text"
        placeholder="Paste description"/>
    </div>
  </div>

  {% include "paste/base_file" %}

  <div id="end_of_files"></div>

  <div class="paste-submit-buttons">
    <div class="field is-grouped">
      <div class="control">
        <button id="add_file" class="button requires-js" type="button">Add file</button>
      </div>
    </div>

    <div class="field is-grouped">
    {% if user %}
      <div class="control">
        <button class="button" name="anonymous" type="submit">Submit anonymously</button>
      </div>
    {% endif %}
      <div class="control">
        <button class="button is-link" type="submit">Submit</button>
      </div>
    </div>
  </div>
</form>
{% endblock main %}
